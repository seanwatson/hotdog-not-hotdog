Hotdog or Not Hotdog
====================

A mobile app inspired by HBO's Silicon Valley SeeFood app ([clip](https://www.youtube.com/watch?v=ACmydtFDTGs))

Can be used to classify whether an object is a hotdog or not a hotdog.

Confused by [hotdog like objects](https://www.youtube.com/shorts/GGJREptWBMU)
